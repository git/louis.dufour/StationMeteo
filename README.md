# Projet Java
Ce projet a été réalisé durant le cours de Java FX de 2ème année de BUT à l'IUT Clermont Auvergne. Cette application vous permet de gérer des capteurs de votre Station Météo.

# Ma configuration
* javafx-sdk-18.0.2
* Option VM : --module-path /home/loulou/Install/javafx-sdk-18.0.2/lib --add-modules=javafx.controls,javafx.fxml

# Utilisation
* Vous devez tirer la branche 'master'
* L'application a été coder sur linux, par conséquent, si vous lancez l'application depuis un autre système d'exploitation, le générateur CPU ne fonctionnera pas

# Respect des consignes
J'ai implémenté trois patrons de conception vu en cour :
* Le patron **Observateur** (pour actualiser les températures des capteurs si changement)
* Le patron **Composite** (pour la création des différents capteurs)
* Le patron **Stratégie** (pour les différents Stratégie de génération de température)

**Tâches** | **Ce qui marche**           | **Ce qui ne marche pas** 
 --- |------------------------| --- 
Visualiser la hiérarchie des capteurs (TreeView) avec possibilité d'en sélectionner un pour voir son détail |X                        | 
Chaque élément de l'arborescence aura un affichage composé d'une icône suivi du nom du capteur |X                        | 
Détail affiché sur la partie droite avec le nom et l'id | X                      |
Dans le cas d'un **capteur simple** son temps de mise à jour via un spinner (modifiable) |            X  (j'ai fais un spinner, mais pour la température)         |
Dans le cas d'un **capteur simple** choix de la Stratégie de génération de températeur (ComboBox) |    X                   |
Dans le cas d'un **capteur simple** un ToggleButton permettant de démarrer/arrêter la génération automatique de la température |                       |X
Dans le cas d'un **capteur composé** tableau montré les fils directs |                       |X
Dans le cas d'un **capteur composé** tableau avec des poids modifiable |  X (j'ai fait les poids, mais pas le tableau)                     |
Boutton d'ajout de capteur |            X           |
Boutton suppression d'un capteur |                     | X 

## Développeur(s) 
* Louis Dufour : Louis.Dufour@etu.uca.fr
