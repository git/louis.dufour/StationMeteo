package model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public abstract class Capteur extends Observable implements Runnable{
   /**** Attributs ****/
    private int id;
    public static int idAct = 1;
    private String nom;
    private Thread thread;
    private int poidCapteur;
    private DoubleProperty temperature;

    /**** Méthodes ****/
    public Capteur(String nom, int poidCapteur){
        this.id = idAct++;
        this.setNom(nom);
        this.temperature = new SimpleDoubleProperty(0);
        this.poidCapteur = poidCapteur;

    }

    public void setTemperature(double temperature) {
        this.temperature.set(temperature);
        notifier();
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getTemperature() {
        return this.temperature.get();
    }

    public DoubleProperty getTemperatureProperty() {
        return this.temperature;
    }

    public String getNom() {
        return nom;
    }

    public int getPoidCapteur() {
        return poidCapteur;
    }

    public void setPoidCapteur(int poidCapteur) {
        this.poidCapteur = poidCapteur;
    }
    public int getId() {
        return id;
    }

    public abstract UnitCapteur getUnitCapteur();

    public abstract CompositeCapteur getCompositeCapteur();

    @Override
    public abstract void run();

    @Override
    public String toString() {
        return this.nom + ' ' + this.id;
    }

}
