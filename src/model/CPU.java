package model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CPU implements GeneratorStrategy{
    @Override
    public double genererTemperature() {
        double temp;
        try {
            FileReader fr = new FileReader("/sys/class/thermal/thermal_zone2/temp");
            temp = (double)fr.read();
            temp = temp;
            return temp;
        } catch (FileNotFoundException e) {
            return -1;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Gen CPU";
    }
}
