package model;

public interface GeneratorStrategy {
    public abstract double genererTemperature();

    @Override
    public String toString();

}
