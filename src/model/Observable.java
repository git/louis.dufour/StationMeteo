package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Observable {
    /**** Attributs ****/
    private List<Observateur> Observables = new ArrayList<>();

    /**** Méthodes ****/
    public void ajouterObservable(Observateur elem)
    {
        Observables.add(elem);
    }

    public void SupprObservable(Observateur elem)
    {
        Observables.remove(elem);
    }

    public void notifier(){
        for (Observateur elem: Observables)
        {
            elem.Update();
        }
    }

    public List<Observateur> getObservables() {
        return Collections.unmodifiableList(Observables);
    }
}
