package model;

import javafx.application.Platform;

import java.util.ArrayList;
import java.util.List;

public class CompositeCapteur extends Capteur {
    /**** Attributs ****/
    private List<Capteur> listCapteur = new ArrayList<Capteur>();
    private GeneratorStrategy genTemp;

    public CompositeCapteur(String nom, int poidCapteur) {
        super(nom, poidCapteur);
    }

    /**** Méthodes ****/

    @Override
    public double getTemperature(){
        double temp = 0;
        int nbcapt = 0;
        for (Capteur capt : listCapteur){
            temp = capt.getTemperature();
            nbcapt++;
        }
        temp = temp/nbcapt;
        return temp;
    }

    public List<Capteur> getListCapteur()
    {
        return listCapteur;
    }

    public void ajouterCapteur(Capteur c){
        listCapteur.add(c);
    }

    @Override
    public CompositeCapteur getCompositeCapteur() {
        return this;
    }

    @Override
    public UnitCapteur getUnitCapteur() {
        return null;
    }

    @Override
    public void run() {
        while(true){
            Platform.runLater(()->setTemperature(this.getTemperature()));
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    @Override
    public String toString() {
        return getNom();
    }
}
