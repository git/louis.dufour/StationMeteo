package model;


import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TreeViewCapteurGenerate {
    public TreeItem<Capteur> CreateTreeItem(Capteur capteur){
        String path="";
        if(capteur.getUnitCapteur() != null){
            path="/img/captor_icon.png";
        }
        if(capteur.getCompositeCapteur() != null){
            path="/img/multi_captor_icon.png";
        }
        TreeItem<Capteur> racine = new TreeItem<>(capteur, new ImageView(new Image(path)));
        racine.setExpanded(true);

        if (capteur.getClass() == CompositeCapteur.class){
            CompositeCapteur composite = (CompositeCapteur)capteur;

            for (Capteur elem: composite.getListCapteur()) {
                if (elem != null) {
                    racine.getChildren().add(CreateTreeItem(elem));
                }
            }
        }
        return racine;
    }
}
