package model;

public class RandomTemp implements GeneratorStrategy{
    /**** Attributs ****/
    private int valMax = 100;
    private int valMin = -272;


    /**** Méthodes ****/
    @Override
    public double genererTemperature() {
        double val = genererTemperatureLimit(valMin, valMax);
        return val;
    }
    public double genererTemperatureLimit(int min, int max){
        double val = (Math.random() * (max - min)) + max;
        return val;
    }

    @Override
    public String toString() {
        return "Aléatoire Température";
    }

}
