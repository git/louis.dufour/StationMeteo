package model;

import javafx.scene.control.Button;
import javafx.stage.Stage;

public abstract class Visualise implements Observateur {
    /**** Attributs ****/
    protected Capteur capteur;

    /**** Méthodes ****/
    public Visualise(Capteur capteur) {
        this.capteur = capteur;
    }

    public void initialize()
    {
        capteur.ajouterObservable(this);
    }
}
