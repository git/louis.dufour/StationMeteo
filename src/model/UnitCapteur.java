package model;

import javafx.application.Platform;

public class UnitCapteur extends Capteur {
    /**** Attributs ****/
    private GeneratorStrategy genTemp;

    /**** Méthodes ****/
    public UnitCapteur(String nom, GeneratorStrategy genTemp, int poidCapteur) {
        super(nom, poidCapteur);
        this.genTemp = genTemp;
    }

    @Override
    public CompositeCapteur getCompositeCapteur() {
        return null;
    }

    @Override
    public UnitCapteur getUnitCapteur() {
        return this;
    }

    @Override
    public void run() {
        while(true){
            Platform.runLater(()->setTemperature(this.genTemp.genererTemperature()));
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    @Override
    public String toString() {
        return getNom();
    }

}
