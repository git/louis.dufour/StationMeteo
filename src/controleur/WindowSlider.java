package controleur;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.Slider;
import model.Capteur;
import model.Visualise;

public class WindowSlider extends Visualise {
    /**** Attributs ****/
    @FXML
    private Slider slider;
    @FXML
    private Label temperature;
    @FXML
    private Button closeButton;

    /**** Méthodes ****/
    public WindowSlider(Capteur capteur) {
        super(capteur);
    }

    public void initialize(){
        super.initialize();
    }

    @FXML
    private void changeTemperature(){
        capteur.setTemperature(slider.getValue());
    }

    @Override
    public void Update() {
        slider.setValue(capteur.getTemperature());
        temperature.setText(String.valueOf(capteur.getTemperature()));
    }

    @FXML
    public void clickClose(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
