package controleur;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WindowMain {
    /**** Attributs ****/
    @FXML
    private ComboBox<GeneratorStrategy> idCbBox;
    @FXML
    private CheckBox idIsCapteurComposite;
    @FXML
    private TreeView<Capteur> idTreeView;
    @FXML
    private HBox champPoid;
    @FXML
    private HBox champCreation;
    @FXML
    private VBox ChampInfoCapteur;
    @FXML
    private HBox champBtCapteur;
    @FXML
    private TableColumn NomColumn;
    @FXML
    private TableColumn IconColumn;
    @FXML
    private TableColumn PoidColumn;
    @FXML
    private TextField idCaptorId;
    @FXML
    private TextField idCaptorName;
    @FXML
    private TableView<Capteur> idTableView;
    @FXML
    private TextField idCaptorPoid;
    private List<Capteur> listCapteur = new ArrayList<Capteur>();
    private ListView<Capteur> listView = new ListView<Capteur>();
    private TreeItem<Capteur> currentCapteur;
    private TreeViewCapteurGenerate treeBuilder = new TreeViewCapteurGenerate();

    /**** Méthodes ****/
    private ObservableList<Capteur> capteurs = FXCollections.observableArrayList(
            captor->new ObservableValue[]{
                    captor.getTemperatureProperty()
            }
    );

    public void initialize(){
        capteurs.addListener((ListChangeListener.Change<? extends Capteur> c) ->{
            while (c.next()){
                if (c.wasUpdated()){
                    listView.refresh();
                }
            }
        });

        Image CompoCaptorImage=new Image("/img/multi_captor_icon.png",20,20,true,true);
        Image UnitCaptorImage=new Image("/img/captor_icon.png",20,20,true,true);


        idCbBox.getItems().add(new RandomTemp());
        idCbBox.getItems().add(new CPU());

        CompositeCapteur cCompo = new CompositeCapteur("id:"+Capteur.idAct +" Capteur composite ", 1);
        UnitCapteur c1 = new UnitCapteur("id:"+Capteur.idAct + " Gen Aléatoire", new RandomTemp(),2);
        UnitCapteur c2 = new UnitCapteur("id:"+Capteur.idAct + " Gen CPU", new CPU(),3);
        cCompo.ajouterCapteur(c1);
        cCompo.ajouterCapteur(c2);

        capteurs.add(cCompo);

        TreeItem<Capteur> base = new TreeItem<>();
        base.setExpanded(true);
        for (Capteur cap: capteurs) {
            if (cap != null) {
                lauchThread(cap);
                base.getChildren().add(treeBuilder.CreateTreeItem(cap));
            }
        }

        idTreeView.setRoot(base);
        AddMode();
    }

    public void lauchThread(Capteur cap){
        Thread threadCapteur = new Thread(cap);
        threadCapteur.start();
        if (cap.getClass() == CompositeCapteur.class){
            CompositeCapteur composite = (CompositeCapteur)cap;
            for (Capteur capteur: composite.getListCapteur()) {
                lauchThread(capteur);
            }
        }
    }

    public void AddAction() {
        Capteur capteur = null;

        if(!idIsCapteurComposite.isSelected()){
            if(idCbBox.getSelectionModel().getSelectedItem() != null)
                capteur = new UnitCapteur("id:"+Capteur.idAct + " "+ idCbBox.getSelectionModel().getSelectedItem().toString(), idCbBox.getSelectionModel().getSelectedItem(),1);
        }
        else{
            capteur = new CompositeCapteur("id:"+Capteur.idAct +" Capteur composite ", 1);
        }

        if(capteur != null)
        {
            lauchThread(capteur);
            if(currentCapteur != null && currentCapteur.getValue() != null && currentCapteur.getValue().getClass() == CompositeCapteur.class){
                ((CompositeCapteur)currentCapteur.getValue()).ajouterCapteur(capteur);
                String path="";
                if(capteur.getUnitCapteur() != null){
                    path="/img/captor_icon.png";
                }
                if(capteur.getCompositeCapteur() != null){
                    path="/img/multi_captor_icon.png";
                }
                currentCapteur.getChildren().add(new TreeItem<>(capteur, new ImageView(new Image(path))));
            }
            else{
                capteurs.add(capteur);
                String path="";
                if(capteur.getUnitCapteur() != null){
                    path="/img/captor_icon.png";
                }
                if(capteur.getCompositeCapteur() != null){
                    path="/img/multi_captor_icon.png";
                }
                idTreeView.getTreeItem(0).getChildren().add(new TreeItem<>(capteur, new ImageView(new Image(path))));
            }
        }
    }

    @FXML
    public void SpinnerAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/vues/Spinner.fxml"));
        fxmlLoader.setController(new WindowSpinner(currentCapteur.getValue()));
        Parent Spinner = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(Spinner);
        stage.setScene(scene);
        stage.initOwner(((Button)actionEvent.getSource()).getScene().getWindow());
        stage.show();
    }
    @FXML
    public void SliderAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/vues/Slider.fxml"));
        fxmlLoader.setController(new WindowSlider(currentCapteur.getValue()));
        Parent slider = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(slider);
        stage.setScene(scene);
        stage.initOwner(((Button)actionEvent.getSource()).getScene().getWindow());
        stage.show();
    }
    @FXML
    public void ImageAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/vues/Image.fxml"));
        fxmlLoader.setController(new WindowImage(currentCapteur.getValue()));
        Parent image = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(image);
        stage.setScene(scene);
        stage.initOwner(((Button)actionEvent.getSource()).getScene().getWindow());
        stage.show();
    }

    @FXML
    public void TreeViewCurrentSelect(){
        currentCapteur = idTreeView.getSelectionModel().selectedItemProperty().getValue();
        if(currentCapteur.getValue() == null){
           return;
        }
        else{
            if(currentCapteur.getValue().getUnitCapteur() != null){
                idCaptorId.setText(Integer.toString(currentCapteur.getValue().getId()));
                idCaptorName.setText(currentCapteur.getValue().getNom());

                ChampInfoCapteur.setManaged(true);
                ChampInfoCapteur.setVisible(true);

                champBtCapteur.setManaged(true);
                champBtCapteur.setVisible(true);

                idTableView.setManaged(false);
                idTableView.setVisible(false);

                champPoid.setManaged(true);
                champPoid.setVisible(true);

                champCreation.setManaged(false);
                champCreation.setVisible(false);
            }
            if(currentCapteur.getValue().getCompositeCapteur() != null)
            {
                idCaptorId.setText(Integer.toString(currentCapteur.getValue().getId()));
                idCaptorName.setText(currentCapteur.getValue().getNom());
                idCaptorPoid.setText(Integer.toString(currentCapteur.getValue().getCompositeCapteur().getPoidCapteur()));

                idTableView.setManaged(true);
                idTableView.setVisible(true);

                champPoid.setManaged(true);
                champPoid.setVisible(true);

                ChampInfoCapteur.setManaged(true);
                ChampInfoCapteur.setVisible(true);

                champCreation.setManaged(false);
                champCreation.setVisible(false);

                champBtCapteur.setManaged(false);
                champBtCapteur.setVisible(false);
            }
        }
    }

    public void CapteurComposite() {
        if(idIsCapteurComposite.isSelected()){
            idCbBox.setManaged(false);
            idCbBox.setVisible(false);
        }
        else{
            idCbBox.setManaged(true);
            idCbBox.setVisible(true);
        }
    }

    public void AddMode() {
        idCaptorId.setText("");
        idCaptorName.setText("");
        idCaptorPoid.setText("");

        ChampInfoCapteur.setManaged(true);
        ChampInfoCapteur.setVisible(true);

        idTableView.setManaged(false);
        idTableView.setVisible(false);

        champPoid.setManaged(true);
        champPoid.setVisible(true);

        champCreation.setManaged(true);
        champCreation.setVisible(true);

        champBtCapteur.setManaged(false);
        champBtCapteur.setVisible(false);
    }
}
