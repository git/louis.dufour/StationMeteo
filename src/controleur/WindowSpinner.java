package controleur;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import model.Capteur;
import model.Visualise;


public class WindowSpinner extends Visualise {
    /**** Attributs ****/
    @FXML
    private Spinner<Double> idSpinner;
    @FXML
    private Label temperature;
    @FXML
    private Button closeButton;
    SpinnerValueFactory.DoubleSpinnerValueFactory doubleSpinner = new SpinnerValueFactory.DoubleSpinnerValueFactory(0.0,500.0,0.0,0.1);

    /**** Méthodes ****/
    public WindowSpinner(Capteur capteur) {
        super(capteur);
    }

    public void initialize(){
        super.initialize();
    }

    @Override
    public void Update() {
        idSpinner.setValueFactory(doubleSpinner);
        idSpinner.getValueFactory().setValue(capteur.getTemperature());
        temperature.setText(String.valueOf(capteur.getTemperature()));
    }

    public void clickClose(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

}
