package controleur;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Capteur;
import model.Visualise;


public class WindowImage extends Visualise {
    /**** Attributs ****/
    @FXML
    private ImageView imageView;
    @FXML
    private Button closeButton;

    /**** Méthodes ****/
    public WindowImage(Capteur c) {
        super(c);
    }

    @Override
    public void initialize(){
        super.initialize();
        imageView.setImage(new Image("/img/neige.jpg"));
    }
    @Override
    public void Update() {
        final int NEIGE_VALUE = 10;
        final int NUAGE_VALUE = 22;
        String imgUrl = "";
        if(capteur.getTemperature() < NEIGE_VALUE){
            imgUrl = "/img/neige.jpg";
        } else if(capteur.getTemperature() < NUAGE_VALUE){
            imgUrl = "/img/nuage.jpg";
        } else {
            imgUrl = "/img/soleil.jpg";
        }
        if(!imageView.getImage().getUrl().endsWith(imgUrl)) {
            imageView.setImage(new Image(imgUrl));
        }
    }

    public void clickClose(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
